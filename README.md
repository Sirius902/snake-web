# Snake Web

The Snake game (again) but this time written in TypeScript and it runs in the web.

Play it [here](https://sirius902.gitlab.io/snake-web/).