import { createCanvas, makeDpiAware } from './canvas.js'
import GameLooper from './GameLooper.js'
import GameState from './GameState.js'
import update from './update.js'
import render from './render.js'

;(function main() {
    const canvas = createCanvas(600, 700)
    const ctx = canvas.getContext('2d')
    makeDpiAware(ctx)

    const looper = new GameLooper(update, render)
    looper.run(GameState.initial(ctx)).then(gs => gs.dispose())
})()
