import Cell from './snake/Cell.js'

export const rangeTo = (end: number): number[] =>
    end > 0 ? Array.from(Array(end).keys()) : []

export const range = (start: number, end: number, step = 1): number[] =>
    rangeTo(Math.ceil((end - start) / step)).map(n => n * step + start)

export function randomElement<T>(xs: T[]): T | null {
    if (xs.length < 1) {
        return null
    } else {
        return xs[Math.floor(Math.random() * xs.length)]
    }
}

export const id = <T>(x: T): T => x

/**
 * Returns an array in reverse order.
 * 
 * This is used instead of Array#reverse because that mutates the original array.
 * @param xs The array to reverse.
 */
export const reversed = <T>(xs: T[]): T[] => xs.slice(0).reverse()

/**
 * Checks if there is one or more duplicates of cell in cells.
 * 
 * This has to exist because object equality in Array<T>#includes is based on reference.
 * @param cells The cells to check for overlap in.
 * @param cell The cell to check for overlap with.
 */
export function overlaps(cells: Cell[], cell: Cell): boolean {
    return cells
        .map(cc => cc.equals(cell))
        .reduce((acc, b) => acc || b, false)
}
