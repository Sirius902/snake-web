export default class NonEmptyArray<T> {
    /* TODO: Probably make this stuff private and access through public properties
    * to prevent mutation. */
    constructor(public head: T, public tail: T[]) {
    }

    toArray(): T[] {
        return [this.head].concat(this.tail)
    }

    slice(start: number, end = this.length): T[] {
        return this.toArray().slice(start, end)
    }

    push(x: T) {
        this.tail.push(x)
    }

    get(i: number): T {
        if (Number.isInteger(i) && i < this.length) {
            if (i == 0) {
                return this.head
            } else {
                return this.tail[i - 1]
            }
        } else {
            throw new Error(`Invalid index: ${i}`)
        }
    }

    get length(): number {
        return 1 + this.tail.length
    }
}
