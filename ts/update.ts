import GameState from './GameState.js'
import GameTime from './GameTime.js'

export default function update(state: GameState, {dt}: GameTime): GameState {
    if (state.gameOver) {
        if (state.km.pressedKeyCodes().includes('Enter')) {
            return GameState.initial(state.ctx)
        } else {
            return state
        }
    }

    state.snake.orient(state.km)
    if (state.moveTimer >= state.moveDelay) {
        if (state.snake.willCrash(state.cellCount)) {
            state.gameOver = true
            return state
        }
        state.snake.move()
        if (state.snake.overlaps()) {
            state.gameOver = true
            return state
        }
        if (state.snake.pieces.head.equals(state.fruit)) {
            state.fruit = GameState.placeFruit(state.snake, state.cellCount)
            state.snake.grow()
            state.score += 1
        }
        state.moveTimer = 0
    }
    state.moveTimer += dt
    return state
}
