import Vector2 from './Vector2.js'

export default class Rect {
    public position: Vector2
    public size: Vector2

    constructor(x: number, y: number, width: number, height: number) {
        this.position = new Vector2(x, y)
        this.size = new Vector2(width, height)
    }
}
