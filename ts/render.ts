import GameState from './GameState.js'
import GameTime from './GameTime.js'
import Cell from './snake/Cell.js'
import Snake from './snake/Snake.js'
import { reversed } from './util.js'
import Rect from './Rect.js'
import Vector2 from './Vector2.js'
import { dimensions } from './canvas.js'

type Context = CanvasRenderingContext2D

/**
 * Transforms a context operation by adding a save and restore
 * to ensure the context's state is reverted afterwards.
 * @param ctx The rendering context.
 * @param f The function to apply with the context.
 */
const useContext = <T>(f: (c: Context) => T) => (ctx: Context) => {
    ctx.save()
    const r = f(ctx)
    ctx.restore()
    return r
}

function cellSize(gameScreen: Rect, cellCount: number): Vector2 {
    const { x, y } = gameScreen.size
    return new Vector2(x / cellCount, y / cellCount)
}

const background = (gameScreen: Rect) => useContext(ctx => {
    const { x: width, y: height } = gameScreen.size
    ctx.fillStyle = '#888'
    ctx.fillRect(0, 0, width, height)
})

const drawBorder = (gameScreen: Rect) => useContext(ctx => {
    const { x: width, y: height } = gameScreen.size
    ctx.strokeStyle = '#333'
    ctx.lineWidth = 10
    ctx.strokeRect(0, 0, width, height)
})

function fillCell(cell: Cell, gameScreen: Rect, cellCount: number, ctx: Context) {
    const { x: w, y: h } = cellSize(gameScreen, cellCount)
    ctx.fillRect(cell.x * w, cell.y * h, w, h)
}

const drawSnake = ({pieces}: Snake, gameScreen: Rect, cellCount: number) => useContext(ctx => {
    const headColor = '#0d0'
    const tailColor = '#060'
    reversed(pieces.tail).forEach((piece, i) => {
        ctx.fillStyle = tailColor
        fillCell(piece, gameScreen, cellCount, ctx)
    })
    ctx.fillStyle = headColor
    fillCell(pieces.head, gameScreen, cellCount, ctx)
})

const drawFruit = (fruit: Cell, gameScreen: Rect, cellCount: number) => useContext(ctx => {
    ctx.fillStyle = '#e04'
    fillCell(fruit, gameScreen, cellCount, ctx)
})

const debugDrawCells = (gameScreen: Rect, cellCount: number) => useContext(ctx => {
    const { x: width, y: height } = gameScreen.size
    ctx.fillStyle = 'black'
    for (let i = width / cellCount; i < width; i += width / cellCount) {
        ctx.fillRect(i, 0, 1, height)
    }
    for (let i = height / cellCount; i < height; i += height / cellCount) {
        ctx.fillRect(0, i, width, 1)
    }
})

const drawGameOverScreen = (gameScreen: Rect) => useContext(ctx => {
    const { x: width, y: height } = gameScreen.size
    ctx.fillStyle = 'white'
    ctx.font = '50px Consolas, monospace'
    ctx.textAlign = 'center'
    ctx.textBaseline = 'middle'
    ctx.fillText('Game over!', width / 2, height / 2 - 50)
    ctx.font = '25px Consolas, monospace'
    ctx.fillText('Press [Enter] to try again.', width / 2, height / 2, width - 60)
})

/**
 * Save for later just in case I decide to implement checking for the maximum score.
 */
const drawWinScreen = (gameScreen: Rect) => useContext(ctx => {
    const { x: width, y: height } = gameScreen.size
    ctx.fillStyle = 'white'
    ctx.font = '35px Consolas, monospace'
    ctx.textAlign = 'center'
    ctx.textBaseline = 'middle'
    ctx.fillText('You won, wow you\'re really good!', width / 2, height / 2 - 35, width - 60)
    ctx.font = '25px Consolas, monospace'
    ctx.fillText('Press [Enter] to play again.', width / 2, height / 2, width - 60)
})

const drawScoreBar = (score: number, gameScreen: Rect) => useContext(ctx => {
    const width = dimensions(ctx).x
    const height = dimensions(ctx).y - gameScreen.size.y
    ctx.fillStyle = '#008'
    ctx.fillRect(0, 0, width, height)
    ctx.fillStyle = 'white'
    ctx.textAlign = 'center'
    ctx.textBaseline = 'middle'
    ctx.font = '25px Consolas, monospace'
    ctx.fillText(`Score: ${score}`, width / 2, height / 2)
})

export default function render(state: GameState, _: GameTime) {
    const { x, y } = state.gameScreen.position
    state.ctx.translate(x, y)
    background(state.gameScreen)(state.ctx)
    drawFruit(state.fruit, state.gameScreen, state.cellCount)(state.ctx)
    drawSnake(state.snake, state.gameScreen, state.cellCount)(state.ctx)
    drawBorder(state.gameScreen)(state.ctx)

    if (state.gameOver) {
        drawGameOverScreen(state.gameScreen)(state.ctx)
    }
    state.ctx.translate(-x, -y)
    drawScoreBar(state.score, state.gameScreen)(state.ctx)
}
