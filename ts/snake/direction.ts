import Vector2 from '../Vector2.js'

export enum Direction {
    Up,
    Down,
    Left,
    Right
}

export function oppositeDirection(d: Direction): Direction {
    switch (d) {
        case Direction.Up:
            return Direction.Down
        case Direction.Down:
            return Direction.Up
        case Direction.Left:
            return Direction.Right
        case Direction.Right:
            return Direction.Left
    }
}

export function movementVector(dir: Direction): Vector2 {
    switch (dir) {
        case Direction.Up:
            return new Vector2(0, -1)
        case Direction.Down:
            return new Vector2(0, 1)
        case Direction.Left:
            return new Vector2(-1, 0)
        case Direction.Right:
            return new Vector2(1, 0)
    }
}
