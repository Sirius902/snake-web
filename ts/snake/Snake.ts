import Cell from './Cell.js'
import { Direction, movementVector, oppositeDirection } from './direction.js'
import KeyboardManager from '../KeyboardManager.js'
import NonEmptyArray from '../NonEmptyArray.js'

export default class Snake {
    constructor(
        public pieces: NonEmptyArray<Cell>,
        public direction: Direction | null,
        public facing: Direction,
        private directionBuf: (Direction | null)[] = [null, null]) {
    }

    get inputtedDirection(): Direction | null {
        return this.directionBuf[1]
    }

    overlaps(): boolean {
        const ps = this.pieces.toArray()
        return ps
            .map(p => ps.filter(pp => pp.equals(p)).length)
            .some(l => l > 1)
    }

    willCrash(cellCount: number): boolean {
        const direction = this.inputtedDirection

        const head = this.pieces.head
        if (head.x <= 0 && direction == Direction.Left ||
            head.x >= cellCount - 1 && direction == Direction.Right ||
            head.y <= 0 && direction == Direction.Up ||
            head.y >= cellCount - 1 && direction == Direction.Down) {
            return true
        } else {
            return false
        }
    }

    move() {
        this.directionBuf[0] = this.directionBuf[1]
        this.direction = this.directionBuf[0]

        if (this.direction === null) {
            return
        } else {
            const v = movementVector(this.direction)
            const head = this.pieces.head.add(v)
            const tail = this.pieces.slice(0, this.pieces.length - 1)
            this.pieces = new NonEmptyArray(head, tail)
            this.facing = this.direction
        }
    }

    orient(km: KeyboardManager) {
        const { direction: current, facing } = this
        const keyStates = km.pressedKeyCodes()
        const opposite = oppositeDirection(facing)

        if (current !== null && keyStates.length != 1 || keyStates.length == 0) {
            return
        }

        let newDirection = this.direction

        switch (keyStates[0]) {
            case 'KeyW':
            case 'ArrowUp':
                newDirection = Direction.Up
                break
            case 'KeyS':
            case 'ArrowDown':
                newDirection = Direction.Down
                break
            case 'KeyA':
            case 'ArrowLeft':
                newDirection = Direction.Left
                break
            case 'KeyD':
            case 'ArrowRight':
                newDirection = Direction.Right
                break
        }

        if (newDirection != opposite) {
            this.directionBuf[1] = newDirection
        }
    }

    grow() {
        const ps = this.pieces
        this.pieces.push(ps.get(ps.length - 1))
    }
}
