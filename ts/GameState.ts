import Cell from './snake/Cell.js'
import Snake from './snake/Snake.js'
import KeyboardManager from './KeyboardManager.js'
import { rangeTo, randomElement, overlaps } from './util.js'
import { Direction } from './snake/direction.js'
import NonEmptyArray from './NonEmptyArray.js'
import Vector2 from './Vector2.js'
import Rect from './Rect.js'

export default class GameState {
    constructor(
        public ctx: CanvasRenderingContext2D,
        public gameScreen: Rect,
        public km: KeyboardManager,
        public snake: Snake,
        public fruit: Cell,
        public score: number,
        public moveDelay: number,
        public moveTimer: number,
        public cellCount: number,
        public gameOver: boolean) {
    }

    dispose() {
        this.km.dispose()
    }

    static initial(ctx: CanvasRenderingContext2D): GameState {
        const cellCount = 13
        const moveDelay = 8 / 60
        const snake = this.initialSnake
        const fruit = this.placeFruit(snake, cellCount)
        const gameScreen = new Rect(0, 100, 600, 600)
        const km = new KeyboardManager(['KeyW', 'KeyS', 'KeyA', 'KeyD', 'ArrowUp', 'ArrowDown', 'ArrowLeft', 'ArrowRight', 'Enter'])
        return new GameState(ctx, gameScreen, km, snake, fruit, 0, moveDelay, moveDelay, cellCount, false)
    }

    private static get initialSnake(): Snake {
        const head = new Vector2(1, 3)
        const tail = [new Vector2(1, 2), new Vector2(1, 1)]
        const snake = new Snake(new NonEmptyArray(head, tail), null, Direction.Down)
        return snake
    }

    private static cells(cellCount: number): Cell[] {
        const to = rangeTo(cellCount)
        return to.flatMap(x => to.map(y => new Vector2(x, y)))
    }

    /**
     * Places the fruit where it won't overlap with the snake.
     * 
     * Can return null if the snake covers the whole board.
     * @param snake The snake of the current game state.
     * @param cellCount The cell count of the game state.
     */
    static placeFruit({pieces}: Snake, cellCount: number): Cell {
        if (pieces.length < 1) {
            throw new Error('Snake is empty and should never be!')
        } else {
            return randomElement(this.cells(cellCount).filter(c => !overlaps(pieces.toArray(), c)))
        }
    }
}
