export default class KeyboardManager {
    private pressed: string[] = []
    private keyDownCallback: (e: KeyboardEvent) => void
    private keyUpCallback: (e: KeyboardEvent) => void

    constructor(private keysToTrack: string[]) {
        this.keyDownCallback = (e) => this.keyDown(e)
        this.keyUpCallback = (e) => this.keyUp(e)
        window.addEventListener('keydown', this.keyDownCallback)
        window.addEventListener('keyup', this.keyUpCallback)
    }

    private changeKeyState(key: string, state: boolean) {
        const filtered = this.pressed.filter((k) => k !== key)
        if (state) {
            this.pressed = [key].concat(filtered)
        } else {
            this.pressed = filtered
        }
    }

    private keyDown(e: KeyboardEvent) {
        if (!this.keysToTrack.includes(e.code)) {
            return
        }

        e.preventDefault()
        this.changeKeyState(e.code, true)
    }

    private keyUp(e: KeyboardEvent) {
        if (!this.keysToTrack.includes(e.code)) {
            return
        }

        this.changeKeyState(e.code, false)
    }

    pressedKeyCodes(): readonly string[] {
        return this.pressed
    }

    dispose() {
        window.removeEventListener('keydown', this.keyDownCallback)
        window.removeEventListener('keyup', this.keyUpCallback)
    }
}
