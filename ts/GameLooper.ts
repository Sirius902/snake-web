import GameTime from './GameTime.js'

/**
 * A game loop runner. Will run a render loop as frequently as possible
 * and an update loop at a customizable frequency, both are provided a time delta
 * and an updatable game state.
 * 
 * Designed around the great article at https://gafferongames.com/post/fix_your_timestep/.
 */
export default class GameLooper<S> {
    private running: boolean
    private time: number
    private updateTime: number
    private accumulator: number
    private state: S
    private finish: (state: S) => void
    /** The promise that is returned once the game stops.
     * Will contain the final state for the game. */
    private promise: Promise<S>

    constructor(
        private update: (state: S, gt: GameTime) => S,
        private render: (state: S, gt: GameTime) => void,
        private dt: number = 1 / 60) {
    }

    private init(state: S) {
        this.running = false
        this.time = 0
        this.updateTime = 0
        this.accumulator = 0
        this.state = state
        this.promise = new Promise((resolve, _) => {
            this.finish = resolve
        })
    }

    private get gameTime(): GameTime {
        return new GameTime(this.updateTime, this.dt)
    }

    private tick(now: number) {
        if (this.running) {
            requestAnimationFrame(this.tick.bind(this))
        }

        const frameTime = Math.min((now - this.time) / 1000, 0.25)

        this.time = now
        this.accumulator += frameTime

        while (this.accumulator >= this.dt) {
            this.state = this.update(this.state, this.gameTime)
            this.updateTime += this.dt
            this.accumulator -= this.dt
        }

        this.render(this.state, this.gameTime)
    }

    async run(state: S): Promise<S> {
        if (!this.running) {
            this.init(state)
            this.running = true
            requestAnimationFrame(this.tick.bind(this))
        } else {
            throw new Error('GameLoop already running!')
        }
        return await this.promise
    }

    stop() {
        this.running = false
        this.finish(this.state)
    }
}
