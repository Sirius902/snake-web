import Vector2 from './Vector2.js'

export function createCanvas(width: number, height: number): HTMLCanvasElement {
    let canvas = document.createElement('canvas')
    canvas.width = width
    canvas.height = height
    document.body.append(canvas)
    return canvas
}

/**
 * Resizes the context's canvas to not look awful on high dpi screens.
 * @param ctx The context being used for the canvas.
 */
export function makeDpiAware(ctx: CanvasRenderingContext2D) {
    let canvas = ctx.canvas
    const dpr = window.devicePixelRatio || 1
    canvas.style.width = canvas.width + 'px'
    canvas.style.height = canvas.height + 'px'
    canvas.width *= dpr
    canvas.height *= dpr
    ctx.scale(dpr, dpr)
}

export function dimensions(ctx: CanvasRenderingContext2D): Vector2 {
    return new Vector2(ctx.canvas.clientWidth, ctx.canvas.clientHeight)
}
