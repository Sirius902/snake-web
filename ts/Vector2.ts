export default class Vector2 {
    constructor(public x: number, public y: number) {
    }

    equals(other: Vector2): boolean {
        return this.x === other.x && this.y === other.y
    }

    add(other: Vector2): Vector2 {
        return new Vector2(this.x + other.x, this.y + other.y)
    }
}
